output "public_ip_address" {
  description = "The Public IP addresses of the Virtual Machines"
  value       = azurerm_public_ip.main.ip_address
}

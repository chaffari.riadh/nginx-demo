variable "location" {
  description = "The Azure location where all resources in this example should be created"
  default     = "westeurope"
}

variable "prefix" {
  description = "The prefix which should be used for all resources in this example"
  default     = "test"
}

variable "admin_username" {
  type      = string
  default   = "adminuser"
  sensitive = true
}

variable "admin_password" {
  type      = string
  sensitive = true
}
